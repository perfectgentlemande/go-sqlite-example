Building:
`docker build -t go-sqlite-example:v0.1.0 .`

Creating a volume:
`docker volume create sqlite-vol`

Running a container:
`docker run --mount source=sqlite-vol,target=/app/db -it go-sqlite-example:v0.1.0`