package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite3"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, err := sqlx.Connect("sqlite3", "db/test.db")
	if err != nil {
		log.Fatal("cannot open sql:", err)
	}
	driver, err := sqlite3.WithInstance(db.DB, &sqlite3.Config{})
	if err != nil {
		log.Fatal("cannot create driver:", err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://migrations", "sqlite3", driver)
	if err != nil {
		log.Fatal("cannot create db instance:", err)
	}
	err = m.Up()
	if err != nil && !strings.Contains(err.Error(), "no change") {
		log.Fatal("cannot migrate db:", err)
	}

	_, err = db.Exec("insert into test (id, value) values (?, ?)", uuid.NewString(), time.Now().String())
	if err != nil {
		log.Fatal("cannot insert:", err)
	}

	res := make([]struct {
		ID    string `db:"id"`
		Value string `db:"value"`
	}, 0)

	err = db.Select(&res, "select id, value from test")
	if err != nil {
		log.Fatal("cannot select:", err)
	}

	fmt.Println(res)
}
