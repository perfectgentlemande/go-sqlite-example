FROM golang:1.17.3-alpine3.14 AS builder

RUN apk update
RUN apk upgrade

# sqlite needs cgo
RUN apk add build-base
COPY . /app
WORKDIR /app
RUN CGO_ENABLED=1 go build -o sqliteapp .

# I still don't get how these routes work
FROM alpine:3.14 AS app
WORKDIR /app
COPY --from=builder /app/migrations /app/migrations
COPY --from=builder /app/sqliteapp /app

CMD ["./sqliteapp"]